;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;;;
;;; Centaur tabs
;;;

;; (use-package! centaur-tabs
;;   :init
;;   (setq centaur-tabs-enable-key-bindings t) ;; (C-c t)

;;   :config
;;   (setq centaur-tabs-show-navigation-buttons t
;;         centaur-tabs-show-count t)
;;   (centaur-tabs-group-by-projectile-project))


;;
;; PlantUML
;;

(after! plantuml
  (setq plantuml-indent-level 2))


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
