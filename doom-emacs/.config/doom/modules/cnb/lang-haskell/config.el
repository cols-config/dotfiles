;;; cnb/lang-haskell/config.el -*- lexical-binding: t; -*-

;; (after! haskell
;;   (setq haskell-interactive-popup-errors nil))

(after! lsp-haskell
  (setq lsp-haskell-formatting-provider "ormolu"))
