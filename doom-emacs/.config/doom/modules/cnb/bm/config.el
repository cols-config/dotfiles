;;; cnb/bm/config.el -*- lexical-binding: t; -*-

(use-package! bm
  :init
  ;; Restore on load.
  (setq bm-restore-repository-on-load t)

  :config
  ;; Keep bookmarks local to buffer. Use native Emacs bookmarks for global bookmarks.
  (setq bm-cycle-all-buffers nil)

  ;; Highlight bookmarks in the left fringe.
  (setq bm-highlight-style 'bm-highlight-only-fringe)
  (setq bm-marker 'bm-marker-left)

  ;; Persist buffer bookmarks
  (setq-default bm-buffer-persistence t)

  ;; Save bookmarks when closing buffer.
  (add-hook 'kill-buffer-hook #'bm-buffer-save)

  ;; Restore a buffers bookmarks when its opened.
  (add-hook 'find-file-hook     #'bm-buffer-restore)
  (add-hook 'after-revert-hook  #'bm-buffer-restore)

  ;; Save bookmarks to disk on exiting Emacs.
  (add-hook 'kill-emacs-hook #'(lambda nil
                                 (bm-buffer-save-all)
                                 (bm-repository-save)))

  (map! "<f2>" #'bm-next)
  (map! "S-<f2>" #'bm-previous)
  (map! "C-<f2>" #'bm-toggle))
