;;; cnb/lang-elixir/config.el -*- lexical-binding: t; -*-

;;;
;;; Elixir Mode
;;;

(after! elixir-mode
  (setq flycheck-elixir-credo-strict t)

  ;; Treat exunit test results, format error and mix compile windows as popups.
  (set-popup-rule! "^\\*exunit-compilation\\*" :size 0.25 :quit 'current)
  (set-popup-rule! "^\\*elixir-format-errors\\*" :size 0.25 :quit 'current)
  (set-popup-rule! "^\\*mix compile\\*" :size 0.25 :quit 'current)

  ;; Wrap exunit error messages
  (add-hook 'exunit-compilation-mode-hook 'turn-on-visual-line-mode)

  (use-package! mix
    :config
    (add-hook 'elixir-mode-hook 'mix-minor-mode)
    (map! :localleader
          :map elixir-mode-map
          :prefix ("m" . "mix")
          "c" #'mix-compile
          "l" #'mix-last-command
          "m" #'mix-execute-task)))

;; (use-package polymode
;;   :mode ("\.ex$" . poly-elixir-web-mode)
;;   :config
;;   (define-hostmode poly-elixir-hostmode :mode 'elixir-mode)
;;   (define-innermode poly-liveview-expr-elixir-innermode
;;     :mode 'web-mode
;;     :head-matcher (rx line-start (* space) "~H" (= 3 (char "\"'")) line-end)
;;     :tail-matcher (rx line-start (* space) (= 3 (char "\"'")) line-end)
;;     :head-mode 'host
;;     :tail-mode 'host
;;     :allow-nested nil
;;     :keep-in-mode 'host
;;     :fallback-mode 'host)
;;   (define-polymode poly-elixir-web-mode
;;     :hostmode 'poly-elixir-hostmode
;;     :innermodes '(poly-liveview-expr-elixir-innermode)))

;; (setq web-mode-engines-alist '(("elixir" . "\\.ex\\'")))
