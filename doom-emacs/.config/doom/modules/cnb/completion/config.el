;;; ../../.dotfiles/doom-emacs/.config/doom/cnb/completion/config.el -*- lexical-binding: t; -*-

(message "completion")

;; Completion annotations.
(after! marginalia
  (setq marginalia-max-relative-age (* 60 60 24 7)) ; 7 day
  (setq marginalia-align 'center))

;; Completion interface.
(after! vertico
  ;; Display completion candidates in a buffer not the mini-buffer.
  ;; (vertico-buffer-mode)

  ;; Once https://github.com/doomemacs/doomemacs/issues/6296 is fixed I
  ;; should be able to remove these lines.
  (setq which-key-use-C-h-commands t)
  (map! :leader "w C-h" nil)

  ;; Don't cycle, nice to know when at end.
  (setq vertico-cycle nil)

  ;; Set a bit of space inside border.
  (setq vertico-posframe-parameters
        '((left-fringe . 8)
          (right-fringe . 8)))
  (setq vertico-posframe-poshandler #'posframe-poshandler-frame-center)

  ;; (setq vertico-buffer-display-action '(display-buffer-in-direction
  ;;                                       (window-height . ,(+ 3 vertico-count))
  ;;                                       (direction . below)))
  ;; (setq vertico-buffer-display-action '(display-buffer-in-side-window
  ;;                                       (window-height . ,(+ 3 vertico-count))
  ;;                                       (side . bottom)))

  ;; Prefix current candidate with "» ".
  (advice-add #'vertico--format-candidate :around
              (lambda (orig cand prefix suffix index start)
                (setq cand (funcall orig cand prefix suffix index start))
                (concat
                 (if (= vertico--index index)
                     (propertize "» " 'face 'vertico-current)
                   "  ")
                 cand))))
