;;; cnb/spelling/config.el -*- lexical-binding: t; -*-

;; Default dictionary for spell checker.
(after! flyspell
  (setq ispell-dictionary "australian")
  (setq flyspell-abbrev-p t))
