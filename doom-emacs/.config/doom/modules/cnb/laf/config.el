;;; ../../.dotfiles/doom-emacs/.config/doom/cnb/theme/config.el -*- lexical-binding: t; -*-

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; Integer font size is pixels, floating point (12.0) is points.
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
(setq doom-font (font-spec :family "Jet Brains Mono" :size 15)
      doom-variable-pitch-font (font-spec :family "IBM Plex Sans" :size 15)
      doom-serif-font (font-spec :family "IBM Plex Serif" :size 15)
      doom-big-font (font-spec :family "Jet Brains Mono" :size 30))

(defvar cnb-themes '(doom-gruvbox doom-gruvbox-light) "Two element list of themes to toggle between")

(defun cnb-toggle-themes ()
  "Toggle between the two themes in `cnb-themes'."
  (declare (interactive-only t))
  (interactive)
  (let ((a (car cnb-themes))
        (b (cadr cnb-themes))
        (cur_theme (car custom-enabled-themes)))
    (disable-theme cur_theme)
    (if (eq cur_theme a)
        (load-theme b)
      (load-theme a))))

(map! "<S-f5>" #'cnb-toggle-themes)
(map! "<f5>" #'modus-themes-toggle)

(setq cnb-splash-images-dir (concat doom-user-dir "images/"))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:

(setq modus-themes-bold-constructs t
      modus-themes-italic-constructs t
      modus-themes-mixed-fonts t
      modus-themes-to-toggle '(modus-operandi modus-vivendi)
      doom-theme 'modus-vivendi
      fancy-splash-image (concat cnb-splash-images-dir "doom-emacs-white.svg"))


(defun cnb/match-splash-image-to-theme(_theme)
  "Try to match the splash image to the current themes background"
  (interactive)
  (if (eq (frame-parameter nil 'background-mode) 'dark)
      (setq fancy-splash-image (concat cnb-splash-images-dir "doom-emacs-white.svg"))
    (setq fancy-splash-image (concat cnb-splash-images-dir "doom-emacs-black.svg")))

  (+doom-dashboard-reload-frame-h nil))

;; (advice-add #'load-theme :after #'cnb/match-splash-image-to-theme )
(advice-add #'enable-theme :after #'cnb/match-splash-image-to-theme )

(setq doom-modeline-icon (display-graphic-p)
      doom-modeline-major-mode-icon t
      doom-modeline-major-mode-color-icon t
      doom-modeline-indent-info t)

(unless (string-match-p "^Power N/A" (battery))   ; On laptops...
  (display-battery-mode 1))                       ; it's nice to know how much power you have

;; Show menu bar
(menu-bar-mode +1)

;; Use the project name and buffer name as Frame title.
(setq frame-title-format
      '(""
        "%b"
        (:eval
         (let ((project-name (projectile-project-name)))
           (unless (string= "-" project-name)
             (format " in [%s]" project-name))))))

;; (after! doom-ui
;;   (if (display-graphic-p)
;;       (progn
;;         Full screen.
;;         (set-frame-parameter nil 'fullscreen 'fullboth)
;;         (toggle-frame-fullscreen)

;;         Show menu-bar
;;         (menu-bar-mode 1)))

;;         Transparent Frames.
;;         (set-frame-parameter (selected-frame) 'alpha '(90 . 90))
;;         (add-to-list 'default-frame-alist '(alpha . (90 . 90))))))
