;; -*- no-byte-compile: t; -*-
;;; cnb/which-key/packages.el

(package! which-key-posframe
  :recipe (:host github :repo "colbell/which-key-posframe"))
