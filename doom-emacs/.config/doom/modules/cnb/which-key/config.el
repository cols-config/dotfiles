;;; cnb/which-key/config.el -*- lexical-binding: t; -*-

;; Hide 'evil-' and 'evilem-' in function names in whichkey menus so we can see
;; the important part of the function name in the menu.
(after! which-key
  (pushnew!
   which-key-replacement-alist
   '(("" . "\\`+?evil[-:]?\\(?:a-\\)?\\(.*\\)") . (nil . "◂\\1"))
   '(("\\`g s" . "\\`evilem--?motion-\\(.*\\)") . (nil . "◃\\1"))))

;; Show which key menu in centre of screen.
(use-package! which-key-posframe
  :if window-system
  :config
  (which-key-posframe-mode)
  (setq which-key-posframe-parameters
        '((left-fringe . 8)
          (right-fringe . 8))))
