;;; cnb/know-your-http-well/config.el -*- lexical-binding: t; -*-

(use-package! know-your-http-well
  :commands (http-header http-method http-relation http-status-code media-type))
