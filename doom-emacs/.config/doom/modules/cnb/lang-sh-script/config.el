;;; cnb/lang-sh-script/config.el -*- lexical-binding: t; -*-

(after! sh-script
  (setq sh-basic-offset 2))
