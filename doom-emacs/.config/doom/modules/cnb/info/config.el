;;; cnb/info/config.el -*- lexical-binding: t; -*-

(use-package! info-colors
  :commands (info-colors-fontify-node)
  :init (add-hook 'Info-selection-hook 'info-colors-fontify-node))
