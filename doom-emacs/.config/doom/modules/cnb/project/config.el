;;; cnb/project/config.el -*- lexical-binding: t; -*-

;; Create new workspaces when switching projects.
(setq +workspaces-on-switch-project-behavior t)

;; Prompt for project buffer on split window and follow.
(defadvice! prompt-for-buffer (&rest _)
  :after #'(+evil/window-split-and-follow +evil/window-vsplit-and-follow)
  (+vertico/switch-workspace-buffer))

;;  This may cause problems. see https://github.com/doomemacs/doomemacs/issues/6205
;; (after! persp
;;   (setq-hook! 'persp-mode-hook uniquify-buffer-name-style 'forward)) ; Unique buffer names.
