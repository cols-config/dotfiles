;;; cnb/lang-ruby/config.el -*- lexical-binding: t; -*-

;; Use bundler for ruby lsp
(after! ruby
  (setq lsp-solargraph-use-bundler t))
