;;; cnb/treemacs/config.el -*- lexical-binding: t; -*-

(use-package! treemacs
  :init
  (setq +treemacs-git-mode 'deferred)

  :config
  (treemacs-follow-mode 1))
