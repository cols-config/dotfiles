;;; cnb/treesit/config.el -*- lexical-binding: t; -*-

(use-package! treesit
  :defer t
  :config
  (setq treesit-font-lock-level 4))
