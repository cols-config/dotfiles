;;; cnb/key-mapping/config.el -*- lexical-binding: t; -*-

;; FIXME: Replace with use-package
(require 'casual-bookmarks)
(require 'casual-dired)
(require 'casual-ibuffer)
(require 'casual-isearch)

(map! :ni "M-m" #'cycle-spacing) ; Set shortcut for cleaning up white-space.

(map! :prefix "<f12>"
      :desc "Casual avy" "a" #'casual-avy-tmenu
      :desc "Casual bookmarks" "b" #'casual-bookmarks-tmenu)

(map! :map isearch-mode-map
      "<f11>" #'casual-isearch-tmenu)

(map! :after dired
      :map dired-mode-map
      :prefix "<f11>"
      :desc "Casual" "m" #'casual-dired-tmenu
      :desc "Search/replace" "/" #'casual-dired-search-replace-tmenu
      :desc "Sort" "s" #'casual-dired-sort-by-tmenu)

(map! :after ibuffer
      :map ibuffer-mode-map
      :prefix "<f11>"
      :desc "Casual" "m" #'casual-ibuffer-tmenu
      :desc "Filter" "f" #'casual-ibuffer-filter-tmenu
      :desc "Sort" "s" #'casual-ibuffer-sortby-tmenu)

(map! :map Info-mode-map
      "<f11>" #'casual-info-tmenu)
