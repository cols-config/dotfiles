;; -*- no-byte-compile: t; -*-
;;; cnb/source-control/packages.el

(package! blamer)
(package! magit-file-icons)
(package! magit-pretty-graph
  :recipe (:host github :repo "georgek/magit-pretty-graph"))
(package! magit-todos)
