;;; cnb/source-control/config.el -*- lexical-binding: t; -*-

(use-package! magit
  :custom
  ;; Is there any point in this?
  ;; (magit-repository-directories cnb-projects-path)

  ;; Show gravatars within commits.
  (magit-revision-show-gravatars '("^Author:     " . "^Commit:     "))

  ;; Show magit in full frame.
  (magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1)

  ;; Lets not wait for large Magit diffs, I.E. large merges.
  (magit-diff-expansion-threshold 5))

(use-package! magit-file-icons
  :after magit
  :init
  (magit-file-icons-mode 1)
  :custom
  (magit-file-icons-enable-diff-file-section-icons t)
  (magit-file-icons-enable-untracked-icons t)
  (magit-file-icons-enable-diffstat-icons t))

(use-package! magit-todos
  :after magit
  :config
  (magit-todos-mode 1)

  ;; Don't want TODOs from third-parties showing up.
  (add-to-list 'magit-todos-exclude-globs "assets/vendor/"))

(use-package! magit-pretty-graph
  :after magit
  :init
  (setq magit-pg-command
        (concat "git --no-pager log"
                " --topo-order --decorate=full"
                " --pretty=format:\"%H%x00%P%x00%an%x00%ar%x00%s%x00%d\""
                " -n 2000")) ;; Increase the default 100 limit

  (map! :localleader
        :map (magit-mode-map)
        :desc "Magit pretty graph" "p" (cmd! (magit-pg-repo (magit-toplevel)))))

(use-package blamer
  :defer 20
  :custom
  (blamer-idle-time 0.8)
  (blamer-min-offset 30)

  :config
  (global-blamer-mode 1)

  :init
  (if (window-system)
      (map! "C-c b" #'blamer-show-posframe-commit-info)
    (map! "C-c b" #'blamer-show-commit-info)))
