;;; cnb/org/config.el -*- lexical-binding: t; -*-

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org")

(use-package! org
  :defer

  :config
  (setq org-src-window-setup 'reorganize-frame)
  (setq org-startup-folded t)
  (setq org-agenda-inhibit-startup nil)

  ;; Org - Allow tab to recursively toggle subtree
  (after! evil-org
    (remove-hook 'org-tab-first-hook #'+org-cycle-only-current-subtree-h)))
