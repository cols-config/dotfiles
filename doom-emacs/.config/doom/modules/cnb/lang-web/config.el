;;; cnb/lang-web/config.el -*- lexical-binding: t; -*-

(after! js2-mode (setq js2-basic-offset 2))

(after! javascript
  (setq javascript-indentation 2
        js-indent-level 2))

;; Set web-mode indentation
(after! web
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2))

;; Overwrite existing scss-stylelint checker to not use --syntax
;; See https://github.com/flycheck/flycheck/issues/1912
;;(flycheck-define-checker scss-stylelint
;;  "A SCSS syntax and style checker using stylelint.
;;
;;See URL `http://stylelint.io/'."
;;  :command ("stylelint"
;;            (eval flycheck-stylelint-args)
;;            ;; "--syntax" "scss"
;;            (option-flag "--quiet" flycheck-stylelint-quiet)
;;            (config-file "--config" flycheck-stylelintrc))
;;  :standard-input t
;;  :error-parser flycheck-parse-stylelint
;;  :modes (scss-mode))

;; scss flycheck checker is in node modules directory
(after! scss
  (add-hook 'scss-mode-hook #'add-node-modules-path))
