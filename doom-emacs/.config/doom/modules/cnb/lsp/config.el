;;; cnb/lsp/config.el -*- lexical-binding: t; -*-

(after! lsp-mode
  (setq lsp-lens-enable nil
        lsp-semantic-tokens-enable t
        lsp-headerline-breadcrumb-enable t)

  ;; Ignore some directories in LSP
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]cover\\'")
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]deps\\'")
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]Mnesia.nonode@nohost\\'"))

(after! lsp-ui
  (setq lsp-ui-sideline-show-hover t
        lsp-ui-imenu-auto-refresh t
        lsp-ui-sideline-show-code-actions t
        lsp-ui-doc-position 'top
        lsp-ui-doc-show-with-cursor nil))
