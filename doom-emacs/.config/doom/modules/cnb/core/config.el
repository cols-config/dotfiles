;;; cnb/core/config.el -*- lexical-binding: t; -*-

;; Location of third-party elisp.
(add-to-list 'load-path  (concat doom-user-dir "elisp/"))

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Colin Noel Bell"
      user-mail-address "col@baibell.org")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'visual) ; works better with code folding etc.

;; When using Emacs on-line help the active point isn’t moved to the new buffer, this fixes that.
(setq help-window-select t)

;; Move deleted files to system trash.
(setq delete-by-moving-to-trash t
      trash-directory nil)

;; Stretch cursor to the Glyph. I.E. If its over a tab it will be very wide.
(setq x-stretch-cursor t)

;; Do not create lock files for files being edited. Amongst other things this
;; fixes problems with code reloading not working in Elixir/Phoenix.
(setq create-lockfiles nil)

;; When saving a script make it executable.
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;; Move mouse cursor away from text cursor. Doesn't work in Wayland.
(mouse-avoidance-mode 'exile)

;; Run Node in development mode
(setenv "NODE_ENV" "development")

;; Work around the error "Remote file error: Forbidden reentrant call of Tramp".
(setq debug-ignored-errors
      (cons 'remote-file-error debug-ignored-errors))

;; Take new window space from all other windows (not just current).
(setq window-combination-resize t)

;; Ctrl-l sequence
(setq recenter-positions '(top middle bottom))

(setq-default abbrev-mode t)

;;
;; Better scrolling when images etc. in buffer.
(use-package! pixel-scroll
  :init (pixel-scroll-precision-mode)
  :custom (pixel-scroll-precision-interpolate-page t))

(after! whitespace
  (setq whitespace-style '(face trailing tabs tab-mark indentation::tab)))
