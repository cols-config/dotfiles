# Some interesting iex cmds.

# a = [1,2,3]
# i a

# h Project.Accounts.get_user/1

# recompile([force: true])

# Add to a project specific .iex.exs
# File.exists?(Path.expand("~/.iex.exs")) && import_file("~/.iex.exs")

# alias Project.Repo

# alias Project.User
# alias Project.Post

import_if_available(Ecto.Query)
import_if_available(Ecto.Changeset)

IEx.configure(history_size: 1024)

# Don't limit number of items in tuples etc.
# Use pretty printing.
# Print all lists as charlists.
IEx.configure(inspect: [limit: :infinity, pretty: true, charlists: :as_lists])

# Default prompt.
IEx.configure(default_prompt:
  [
    # ANSI CHA, move cursor to column 1
    # "\e[G",
    :blue,
    # plain string
    "🧪 iex",
    "»",
    :white,
    :reset
  ]
  |> IO.ANSI.format()
  |> IO.chardata_to_string()
)
#+END_SRC

# Prompt for an active node
IEx.configure(alive_prompt:
  [
    # ANSI CHA, move cursor to column 1
    # "\e[G",
    :blue,
    # plain string
    "»",
    "🧪 iex",
    :yellow,
    " (%node)",
    :light_magenta,
    ">",
    :white,
    :reset
  ]
  |> IO.ANSI.format()
  |> IO.chardata_to_string()
)
