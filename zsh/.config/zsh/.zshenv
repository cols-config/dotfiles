#!/usr/bin/env zsh

export DOTFILES="$HOME/.dotfiles"

# Editor
export EDITOR='vim'
export VISUAL='vim'
#export VISUAL='emacsclient'

# History
HISTFILE="${ZDOTDIR:-$HOME}/.zsh_history"       # The path to the history file.
HISTSIZE=100000              # The maximum number of events to save in the internal history.
SAVEHIST=100000              # The maximum number of events to save in the history file.

# System mail location.
export MAIL=/var/mail/$USER

# Set man display width
export MANWIDTH=96

# Enable history in IEX
export ERL_AFLAGS="-kernel shell_history enabled"

#??
#export GTAGSLABEL=pygments

# Put private bin directories in path if they exist and not already in path.
if [ -d "$HOME/bin" ] && [[ ":$PATH:" != *":$HOME/bin:"* ]] ; then
  PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] && [[ ":$PATH:" != *":$HOME/.local/bin:"* ]] ; then
  PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/go/bin" ] && [[ ":$PATH:" != *":$HOME/go/bin:"* ]] ; then
  PATH="$HOME/go/bin:$PATH"
fi
if [ -d "$HOME/.node_modules/bin" ] && [[ ":$PATH:" != *":$HOME/.node_modules/bin:"* ]] ; then
  PATH="$HOME/.node_modules/bin:$PATH"
fi
if [ -d "$HOME/elixir-ls/release" ] && [[ ":$PATH:" != *":$HOME/elixir-ls/release:"* ]] ; then
  PATH="$HOME/elixir-ls/release:$PATH"
fi
if [ -d "$HOME/.config/emacs/bin" ] && [[ ":$PATH:" != *":$HOME/.config/emacs/bin:"* ]] ; then
  PATH="$PATH:$HOME/.config/emacs/bin"
fi

if [ -d "$HOME/.config/antidote" ] && [[ ":$PATH:" != *":$HOME/.config/antidote:"* ]] ; then
  PATH="$HOME/.config/antidote:$PATH"
fi
if [ -d /opt/nvim-linux64/bin ] && [[ ":$PATH:" != *":/opt/nvim-linux64/bin:"* ]] ; then
  PATH="$PATH:/opt/nvim-linux64/bin"
fi
export PATH


if [ -x "$HOME/.cargo/env" ] && [[ ":$PATH:" != *":$HOME/.cargo/env:"* ]] ; then
  . "$HOME/.cargo/env"
fi
