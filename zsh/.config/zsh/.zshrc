#!/usr/bin/env zsh

# Zsh always executes zshenv. Then, depending on the case:
# - run as a login shell, it executes zprofile;
# - run as an interactive, it executes zshrc;
# - run as a login shell, it executes zlogin.


# +------------------+
# | Show system info |
# +------------------+

fastfetch

# +------------------+
# | Plugin Manager   |
# +------------------+

if [ -f "/usr/share/zsh-antidote/antidote.zsh" ] ; then
  source /usr/share/zsh-antidote/antidote.zsh
  antidote load
elif [ -f "${ZDOTDIR}/.antidote/antidote.zsh" ] ; then
  source "${ZDOTDIR}/.antidote/antidote.zsh"
  antidote load
fi

# +------------------+
# | Powerlevel10k    |
# +------------------+

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialisation code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#   source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
# fi

[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# +------------------+
# | Version Managers |
# +------------------+

if [ -d "$HOME/.asdf" ]; then
  . $HOME/.asdf/asdf.sh
  fpath=(${ASDF_DIR}/completions $fpath)
elif [ -f "/etc/profile.d/rvm.sh" ]; then
  source "/etc/profile.d/rvm.sh"
fi

# +--------------+
# | Shell prompt |
# +--------------+
export TYPEWRITTEN_PROMPT_LAYOUT="pure_verbose"
# export TYPEWRITTEN_SYMBOL="->"

autoload -Uz promptinit
promptinit
prompt typewritten
# prompt powerlevel10k
# prompt adam1

# Autoload zsh's `add-zsh-hook` and `vcs_info` functions
# (-U autoload w/o substition, -z use zsh style)
autoload -Uz add-zsh-hook vcs_info

# Set prompt substitution so we can use the vcs_info_message variable
setopt prompt_subst

# Run the `vcs_info` hook to grab git info before displaying the prompt
add-zsh-hook precmd vcs_info

# Style the vcs_info message
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*' formats '%b%u%c'
# Format when the repo is in an action (merge, rebase, etc)
zstyle ':vcs_info:git*' actionformats '%F{14}⏱ %*%f'
zstyle ':vcs_info:git*' unstagedstr '*'
zstyle ':vcs_info:git*' stagedstr '+'
# This enables %u and %c (unstaged/staged changes) to work,
# but can be slow on large repos
zstyle ':vcs_info:*:*' check-for-changes true

# Only show host name if ssh.
# if [[ -n $SSH_CONNECTION ]]; then
#   myhost="@%{$fg_bold[black]%}%m"
# else
#   myhost=""
# fi
# PROMPT='%K{cyan}%n$myhost%k %(?.%F{14}⏺.%F{9}⏺)%f %2~ %# '

# # Set the right prompt to the vcs_info message
# RPROMPT='%F{8}⎇ $vcs_info_msg_0_%f %F{8}⏱ %*%f'


# +---------+
# | HISTORY |
# +---------+

setopt BANG_HIST              # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY       # Write the history file in the ':start:elapsed;command' format.
setopt INC_APPEND_HISTORY     # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY          # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS       # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS   # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS      # Do not display a line previously found.
setopt HIST_IGNORE_SPACE      # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS      # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS     # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY            # Do not execute immediately upon history expansion.

# Use modern completion system
autoload -Uz compinit && compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# Source VTE for Tilix. See https://gnunn1.github.io/tilix-web/manual/vteconfig/
# You may have to create a symlink.
#
# ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
  if [ -f "/etc/profile.d/vte.sh" ]; then
    source /etc/profile.d/vte.sh
  fi
fi

# if [ "$XDG_SESSION_DESKTOP" = COSMIC ]; then
  # eval $(ssh-agent)
# fi

# Ensure that Emacs tramp can see the prompt char.
if [ "$TERM" = dumb  ]; then
  unsetopt zle prompt_cr prompt_subst &&
  typeset -g POWERLEVEL9K_PROMPT_CHAR_{OK,ERROR}_VIINS_CONTENT_EXPANSION='$' &&
  export PS1='$ ' &&
  return
fi

# Allow less to work on compressed files.
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Setting this on means that if a command is issued that can’t be executed
# as a normal command, and the command is the name of a directory, perform the
# cd command to that directory.
unsetopt AUTO_CD

# Load private envvars
if [ -f "$HOME/.cnb-envvars" ]; then
  source $HOME/.cnb-envvars
fi

source "${ZDOTDIR:-$HOME}/zsh-aliases"
source "${ZDOTDIR:-$HOME}/zsh-functions"

# Load direnv
# eval "$(direnv hook bash)"

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Haskell env
[ -f "/home/col/.ghcup/env" ] && source "/home/col/.ghcup/env" # ghcup-env

# Always show scrollbars in Gnome.
# The default behaviour of only showing on mouse-over is incredibly irritating.
export GTK_OVERLAY_SCROLLING=0
export GTK3_OVERLAY_SCROLLING=0
export GTK4_OVERLAY_SCROLLING=0
