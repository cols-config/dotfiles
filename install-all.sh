#!/usr/bin/env sh

# Loop thru all immed sub-dirs
for str in ./*/;
do
  # Retrieve the package name from the dir. E.G. turn ./asdf/ to asdf
  # and then run stow passing the package name.
  dir=$(basename "$str")
  stow "$dir"
done
